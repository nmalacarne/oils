#! /usr/bin/env bash

# working directories for projects
OPENSRF_ROOT="$HOME/OpenSRF"
EVERGREEN_ROOT="$HOME/Evergreen"

export DEBIAN_FRONTEND=noninteractive # https://serverfault.com/a/670688
export NODE_OPTIONS="--max-old-space-size=8192" # https://stackoverflow.com/a/59572966

sudo apt-get update && sudo apt-get upgrade -y

sudo apt-get install -y \
	git \
	make \
	autoconf \
	automake \
	libtool 

# set up opensrf
# http://evergreen-ils.org/documentation/install/OpenSRF/README_3_2_1.html
if [[ ! -d "$OPENSRF_ROOT" ]]; then
	git clone git://git.evergreen-ils.org/OpenSRF.git "$OPENSRF_ROOT"
fi

cd "$OPENSRF_ROOT"

echo "3 - Installing prerequisites"
sudo make -f src/extras/Makefile.install ubuntu-bionic

echo "4 - Developer instructions"
autoreconf -i

echo "5 - Configuration and compilation instructions"
./configure \
	--prefix=/openils \
	--sysconfdir=/openils/conf \
	--with-websockets-port=443 
make

echo "6 - Installation instructions"
sudo make install

echo "7 - Create and set up the opensrf Unix user environment"
if ! grep -q '^opensrf' /etc/passwd; then
	sudo useradd -m -s /bin/bash opensrf
	echo -e "password\npassword" | sudo passwd opensrf
	sudo sh -c 'echo "export PATH=\$PATH:/openils/bin" >> /home/opensrf/.bashrc'
	sudo chown -R opensrf:opensrf /openils
fi

echo "8 - Define your public and private OpenSRF domains"
grep -q '127.0.1.2' /etc/hosts || sudo su -c 'echo "127.0.1.2       public.localhost        public" >> /etc/hosts'
grep -q '127.0.1.3' /etc/hosts || sudo su -c 'echo "127.0.1.3       private.localhost       private" >> /etc/hosts'

echo "9 - Adjust the system dynamic library path"
if [[ ! -f /etc/ld.so.conf.d/opensrf.conf ]]; then
	sudo su -c 'echo "/openils/lib" > /etc/ld.so.conf.d/opensrf.conf'
	sudo ldconfig
fi

echo "10 - Configure the ejabberd server"
sudo systemctl stop ejabberd.service
sudo grep -q 'private.localhost' || sudo sed -ir '/^hosts:/a \ \ - "private.localhost"' /etc/ejabberd/ejabberd.yml
sudo grep -q 'public.localhost' || sudo sed -ir '/^hosts:/a \ \ - "public.localhost"' /etc/ejabberd/ejabberd.yml
sudo sed -ir 's/starttls_required:.*/starttls_required: false/' /etc/ejabberd/ejabberd.yml
sudo sed -ir 's/auth_password_format:.*/auth_password_format: plain/' /etc/ejabberd/ejabberd.yml
sudo sed -ir 's/^\s{2}normal:.*/\ \ normal: 500000/' /etc/ejabberd/ejabberd.yml
sudo sed -ir 's/^\s{2}fast:.*/\ \ fast: 500000/' /etc/ejabberd/ejabberd.yml
sudo sed -ir 's/max_user_sessions:.*/max_user_sessions: 10000/' /etc/ejabberd/ejabberd.yml
sudo sed -ir 's/[*##*]mod_offline.*/##\ &/' /etc/ejabberd/ejabberd.yml
sudo sed -ir 's/[*##*]access_max_user_messages.*/##\ &/' /etc/ejabberd/ejabberd.yml
sudo systemctl start ejabberd.service


echo "11 - Create the OpenSrf Jabber users"
jabber_users=( router opensrf)
for jabber_user in "${jabber_users[@]}"; do
	if ! sudo ejabberdctl check_account "$jabber_user" private.localhost; then
		sudo ejabberdctl register "$jabber_user" private.localhost password
	fi

	if ! sudo ejabberdctl check_account "$jabber_user" public.localhost; then
		sudo ejabberdctl register "$jabber_user" public.localhost password
	fi
done

echo "12.2.1 - Updating the OpenSRF configuration files"
cd /openils/conf
sudo su opensrf -c 'cp opensrf_core.xml.example opensrf_core.xml'
sudo su opensrf -c 'cp opensrf.xml.example opensrf.xml'

echo "13 - Starting and stopping OpenSRF services"
sudo su opensrf -c '/openils/bin/osrf_control --localhost --start-all'

echo "15.1 - Install websocketd"
cd /tmp
if [[ ! -f /usr/local/bin/websocketd ]]; then
	wget -nv 'https://github.com/joewalnes/websocketd/releases/download/v0.3.0/websocketd-0.3.0-linux_amd64.zip'
	sudo unzip websocketd-0.3.0-linux_amd64.zip websocketd -d /usr/local/bin
fi

echo "16.1 - Enable remoteip"
sudo a2enmod remoteip
